
const routes = [
  {
    path: "/test",
    component: () => import("layouts/BlankLayout.vue"),
    children: [{path: "", component: () => import("pages/test.vue")}]
  },
  {
    path: '/',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', component: () => import('pages/PageHome.vue') },
      { path: 'login', component: () => import('pages/auth/PageLogin.vue') },
      { path: 'signup', component: () => import('pages/auth/Signup.vue') },
      { path: 'change_password', component: () => import('pages/auth/ChangePassword.vue'), meta: { authRequired: true } },
      { path: 'recover_password', component: () => import('pages/auth/RecoverPassword.vue') },
      { path: 'verify_email', component: () => import('pages/auth/VerifyEmail.vue') },
      { path: 'some_protected_page', component: () => import('pages/SomeProtectedPage.vue'), meta: { authRequired: true } },
      { path: 'update_login', component: () => import('pages/auth/UpdateLogin.vue') }
    ]
  },
  {
    path: "/auth",
    component: () => import("layouts/BlankLayout.vue"),
    children: [{path: "", component: () => import("pages/auth/PageAuth.vue")}]
  },
  {
    path: "/reset",
    component: () => import("layouts/BlankLayout.vue"),
    children: [{path: "", component: () => import("pages/auth/PageReset.vue")}]
  },
  {
    path: "/me",
    component: () => import("layouts/StaticLayout.vue"),
    children: [
      {path: "", component: () => import("pages/profile/PageProfile.vue")}
    ]
  },
  {
    path: "/about",
    component: () => import("layouts/StaticLayout.vue"),
    children: [{path: "", component: () => import("pages/basic/PageAbout.vue")}]
  },
  {
    path: "/faq",
    component: () => import("layouts/StaticLayout.vue"),
    children: [{path: "", component: () => import("pages/basic/PageFaq.vue")}]
  },
  {
    path: "/help",
    component: () => import("layouts/StaticLayout.vue"),
    children: [{path: "", component: () => import("pages/basic/PageHelp.vue")}]
  },
  {
    path: "/tos",
    component: () => import("layouts/StaticLayout.vue"),
    children: [{path: "", component: () => import("pages/basic/PageTos.vue")}]
  },
  {
    path: "/promos",
    component: () => import("layouts/DynamicLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/promo/PagePromolist.vue")
      },
      {
        path: ":id",
        component: () => import("pages/promo/PagePromoitem.vue")
      }
    ]
  },
  {
    path: "/treatments",
    component: () => import("layouts/DynamicLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/treatment/PageTreatmentlist.vue")
      },
      {
        path: ":id",
        component: () => import("pages/treatment/PageTreatmentitem.vue")
      }
    ]
  },
  {
    path: "/shop",
    component: () => import("layouts/DynamicLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/shop/PageShoplist.vue")
      },
      {
        path: ":id",
        component: () => import("pages/shop/PageShopitem.vue")
      }
    ]
  },
  {
    path: "/updates",
    component: () => import("layouts/DynamicLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/news/PageNewslist.vue")
      },
      {
        path: ":id",
        component: () => import("pages/news/PageNewsitem.vue")
      }
    ]
  },
  {
    path: "/outlets",
    component: () => import("layouts/DynamicLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/outlet/PageOutlet.vue")
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
