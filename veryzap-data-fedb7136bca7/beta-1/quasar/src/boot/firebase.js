import firebase from 'firebase/app'
import FirebaseConfig from '../configs/firebase-config.template.js'

export default async ({ Vue }) => {
  firebase.initializeApp(FirebaseConfig)
}
