import VueNumericInput from 'vue-numeric-input';

// leave the export, even if you don't use it
export default async ({ app, router, Vue }) => {
  Vue.use(VueNumericInput);
}
