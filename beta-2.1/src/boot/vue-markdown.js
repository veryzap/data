import VueMarkdown from "vue-markdown";

export default ({app, Vue}) => {
  Vue.use(VueMarkdown);
};
